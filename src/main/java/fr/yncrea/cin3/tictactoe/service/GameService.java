package fr.yncrea.cin3.tictactoe.service;

import fr.yncrea.cin3.tictactoe.domain.CellStatus;
import fr.yncrea.cin3.tictactoe.domain.Game;
import fr.yncrea.cin3.tictactoe.domain.GameStatus;
import org.springframework.stereotype.Service;

@Service
public class GameService {
    /**
     * Create a new game
     *
     * @return a new clean game
     */
    public Game create() {
        Game game = new Game();

        game.setStatus(GameStatus.STARTED);
        game.setCurrentPlayer(CellStatus.X);

        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 3; ++col) {
                setCell(game, row, col, CellStatus.EMPTY);
            }
        }

        return game;
    }

    /**
     * Play at given row and column for the game
     *
     * @param game
     * @param row
     * @param column
     * @return Updated game (not cloned)
     */
    public Game play(Game game, int row, int column) {
        // ok, so this is a valid move, update board
        setCell(game, row, column, game.getCurrentPlayer());

        // check if there is a winner
        checkWinner(game);

        // no winner ? then check if this is a draw
        if (game.getWinner() == null) {
            checkDraw(game);
        }

        // if the game has not ended, alternate player
        alternatePlayer(game);

        return game;
    }

    /**
     * Get a cellStatus at the given row and column of the board
     *
     * @param game
     * @param row
     * @param column
     * @return
     */
    private CellStatus getCell(Game game, int row, int column) {
        return game.getBoard()[row][column];
    }

    /**
     * Get the cellStatus at the given row and column of the board
     * @param game
     * @param row
     * @param column
     * @param value
     */
    private void setCell(Game game, int row, int column, CellStatus value) {
        game.getBoard()[row][column] = value;
    }

    /**
     * Check if there is a winner in the game, and update the status and the winner accordingly
     *
     * @param game
     */
    private void checkWinner(Game game) {
    }

    /**
     * Check if the game is a draw, and update the game status accordingly
     *
     * @param game
     */
    private void checkDraw(Game game) {
    }

    /**
     * Update the game with the next player
     *
     * @param game
     */
    private void alternatePlayer(Game game) {
        if (game.getCurrentPlayer() == CellStatus.O) {
            game.setCurrentPlayer(CellStatus.X);
        } else {
            game.setCurrentPlayer(CellStatus.O);
        }
    }
}
