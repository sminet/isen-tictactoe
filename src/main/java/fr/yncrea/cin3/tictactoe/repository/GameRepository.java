package fr.yncrea.cin3.tictactoe.repository;

import fr.yncrea.cin3.tictactoe.domain.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
