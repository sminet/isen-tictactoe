package fr.yncrea.cin3.tictactoe.domain;

public enum CellStatus {
    EMPTY(""), X("X"), O("O");

    public final String string;

    private CellStatus(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return string;
    }
}
