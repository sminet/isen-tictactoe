package fr.yncrea.cin3.tictactoe.domain;

public enum GameStatus {
    STARTED, DRAW, FINISHED
}
