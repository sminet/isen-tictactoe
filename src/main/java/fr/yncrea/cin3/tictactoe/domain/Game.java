package fr.yncrea.cin3.tictactoe.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
public class Game {
    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    private GameStatus status;

    @Enumerated(EnumType.STRING)
    private CellStatus winner;

    @Enumerated(EnumType.STRING)
    private CellStatus currentPlayer;

    @Lob
    private CellStatus[][] board;

    public Game() {
        board = new CellStatus[3][3];
    }
}
